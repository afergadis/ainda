import pandas as pd
from rdflib import Graph, Namespace, BNode, Literal, URIRef
from rdflib.namespace import RDF, RDFS, XSD

if __name__ == '__main__':
    g = Graph()
    country_ns = Namespace('http://map.news/wdi/country/')
    alpha2 = URIRef('http://map.news/wdi/country#alpha2', base=country_ns)
    alpha3 = URIRef('http://map.news/wdi/country#alpha3', base=country_ns)
    df = pd.read_csv(
        'data/WDICountry.csv',
        index_col='Country Code',
        usecols=['Country Code', 'Short Name', '2-alpha code'])
    print(df.head())
    limit = 0
    for row in df.iterrows():
        g.add((country_ns[row[0]], RDF.type, RDFS.Class))
        g.add((country_ns[row[0]], RDFS.label,
               Literal(row[1]['Short Name'], datatype=XSD.term('string'))))
        g.add((country_ns[row[0]], alpha2,
               Literal(row[1]['2-alpha code'], datatype=XSD.term('string'))))
        g.add((country_ns[row[0]], alpha3,
               Literal(row[0], datatype=XSD.term('string'))))
        limit -= 1
        if limit == 0:
            break

    g.serialize('data/countries.ttl', format='turtle')
