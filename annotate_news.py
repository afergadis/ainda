import spacy
import json
import pycountry
from geopy.geocoders import GeoNames
from tqdm import tqdm

nlp = spacy.load('en_core_web_sm')
gn = GeoNames(username='afergadis')

try:
    p = open('news_progress').read()
    progress = int(p)
except FileNotFoundError:
    progress = 0

pbar = tqdm(total=200853 - progress, smoothing=0, ncols=80)
with open('data/news_dataset.json', 'a') as fp:
    for idx, line in enumerate(open('data/News_Category_Dataset_v2.json')):
        if idx < progress:
            continue
        countries = set()
        record = json.loads(line)
        article = f'{record["headline"]}. {record["short_description"]}'
        doc = nlp(article)
        for ent in doc.ents:
            if ent.label_ == 'GPE':
                try:
                    loc = gn.geocode(ent)
                except:
                    with open('news_progress', 'w') as fh:
                        progress += pbar.n
                        fh.write(f'{progress}')
                    raise
                if loc is None:
                    continue
                try:
                    country = pycountry.countries.get(
                        alpha_2=loc.raw['countryCode'])
                    countries.add(country.alpha_3)
                except KeyError:
                    pass
            pbar.update()
        if len(countries) > 0:
            record['country_code'] = list(countries)
            fp.write(
                json.dumps(
                    record, ensure_ascii=False, sort_keys=True))
            fp.write('\n')
pbar.close()
