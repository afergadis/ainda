import json
import spacy
import pycountry
from string import printable
from ibm_watson import NaturalLanguageUnderstandingV1
from ibm_watson.natural_language_understanding_v1 import Features, EntitiesOptions
from geopy.geocoders import GeoNames
from tqdm import tqdm


def to_printable(s):
    chr = [c for c in s if c in printable]
    return ''.join(chr)


nlu = NaturalLanguageUnderstandingV1(
    version='2019-07-12',
    iam_apikey='797vdKD2pBr5_oJT7ldcKnJCVTJnkhpVFP-QcXNBQVKJ',
    url=
    'https://gateway-lon.watsonplatform.net/natural-language-understanding/api'
)
gn = GeoNames(username='afergadis')
nlp = spacy.load('en_web_lg')

try:
    p = open('news_progress').read()
    progress = int(p)
except FileNotFoundError:
    progress = 0

pbar = tqdm(total=200853 - progress, smoothing=0, ncols=80)
with open('data/news_dataset.json', 'a') as fp:
    for idx, line in enumerate(open('data/News_Category_Dataset_v2.json')):
        if idx < progress:
            continue
        countries = set()
        record = json.loads(line)
        text = f"{record['headline']}. {record['short_description']}"
        try:
            s = to_printable(text)
            if len(s) == 0:
                continue
            response = nlu.analyze(
                text=s,
                features=Features(entities=EntitiesOptions())).get_result()
            for entity in response['entities']:
                if entity['type'] == 'Location':
                    loc = gn.geocode(entity['text'])
                    if loc is None or loc.raw['fcode'] in (
                            'CONT',  # Continent
                            'OCN',  # Ocean
                            'RGN',  # Region
                            'BSNU',  # Basin
                            'FANU',  # Fan
                            'SEA',
                            'PCLH',  # Historical political entity
                    ):
                        continue
                    country = pycountry.countries.get(
                        alpha_2=loc.raw['countryCode'])
                    countries.add(country.alpha_3)
                    if len(countries) > 0:
                        record['country_code'] = list(countries)
                        fp.write(
                            json.dumps(
                                record, ensure_ascii=False, sort_keys=True))
                        fp.write('\n')
        except:
            with open('news_progress', 'w') as fh:
                progress += pbar.n
                fh.write(f'{progress}')
            raise
        pbar.update()
pbar.close()
