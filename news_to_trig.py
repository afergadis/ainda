import json

authors = dict()
categories = dict()
news_links = dict()
with open('data/news.trig', 'w') as fo:
    fo.write("""
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xml:  <http://www.w3.org/XML/1998/namespace>.
@prefix xsd:  <http://www.w3.org/2001/XMLSchema#>.
@prefix wdi:  <http://test.org.newsmap/wdi#>.
@prefix news: <http://test.org.newsmap/news#>.
@prefix hp:   <https://www.huffingtonpost.com/entry/>.

<http://test.org.newsmap/news> {
    news:Link rdf:type rdfs:Class.
    news:Author rdf:type rdfs:Class.
    news:Category rdf:type rdfs:Class.
    
    news:has_link rdf:type rdf:Property.

    news:has_author rdf:type rdf:Property.
    news:has_author rdfs:domain news:Link.
    news:has_author rdfs:range news:Author.
    
    news:has_category rdf:type rdf:Property.
    news:has_category rdfs:domain news:Link.
    news:has_category rdfs:range news:Category.
    
""")
    for line in open('data/news_dataset.json'):
        record = json.loads(line)
        author_uri = ''.join(
            c for c in record['authors'] if c not in [',', "'", '.', ';'])
        author_uri = author_uri.replace(' ', '_')
        authors[author_uri] = record['authors']
        category_uri = record['category'].replace(' ', '_')
        categories[category_uri] = record['category']
        news_link = record['link'].split('/')[-1]
        news_links[news_link] = {
            'article': f'{record["headline"]}. {record["short_description"]}',
            'author_uri': author_uri if len(author_uri) > 0 else None,
            'category_uri': category_uri,
            'countries': record['country_code']
        }
    # Add info about the news_links
    for uri, data in news_links.items():
        # The news_link has a category
        fo.write(f'\thp:{uri}\n'
                 f'\t\trdf:type news:Link;\n'
                 f'\t\tnews:has_category news:{data["category_uri"]};')
        # and an author (not always!)
        if data['author_uri'] is not None:
            fo.write(f'\t\tnews:has_author news:{data["author_uri"]};')
            # and an article (title + short_description)
        countries = ', '.join(data['countries'])
        fo.write(f'\t\twdi:countryCode "{countries}";')
        article = data['article'].replace('"', '\\"')
        fo.write(f'\t\tnews:article "{article}"^^xsd:string.\n')
    # Name the author nodes.
    for uri, author in authors.items():
        fo.write(f'\tnews:{uri}')
        fo.write(f'\t\trdf:type news:Author;')
        fo.write(f'\t\tnews:author_name "{author}";')
        fo.write(f'\t\trdfs:label "{author}"^^xsd:string.\n')
    # Name the category nodes.
    for uri, category in categories.items():
        fo.write(f'\tnews:{uri}')
        fo.write(f'\t\trdf:type news:Category ;')
        fo.write(f'\t\tnews:category_name "{category}";')
        fo.write(f'\t\trdfs:label "{category}"^^xsd:string.\n')
    fo.write('}')
