import pandas as pd
from rdflib import Graph, Namespace, BNode, Literal, URIRef
from rdflib.namespace import RDF, RDFS, XSD

if __name__ == '__main__':
    g = Graph()
    indicator_ns = Namespace('http://map.news/wdi/indicator/')
    df = pd.read_csv(
        'data/WDISeries.csv',
        index_col='Series Code',
        usecols=['Series Code', 'Indicator Name'])
    print(df.head())
    limit = 0
    for row in df.iterrows():
        g.add((indicator_ns[row[0]], RDF.type, RDFS.Class))
        g.add((indicator_ns[row[0]], RDFS.label,
               Literal(row[1]['Indicator Name'], datatype=XSD.term('string'))))
        limit -= 1
        if limit == 0:
            break

    g.serialize('data/indicators.ttl', format='turtle')
